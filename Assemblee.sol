pragma solidity >=0.4.22 <0.6.0;

contract Assemblee {

    address[] private membres;

    function rejoindre() public {
        membres.push(msg.sender);
    }

    function estMembre(address _utilisateur) public view returns (bool) {
        for(uint i=0; i<membres.length; i++){
            if(membres[i] == _utilisateur){
                return true;
            }
        }
        return false;
    }
}
